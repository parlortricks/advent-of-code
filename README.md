# What is this?
This is my repo for tracking my coding progress in the Advent of Code puzzles. I am using the Racket language, as I wanted to learn something new and challenge myself at the same time. I chose Racket to learn a lisp/scheme language, and it seemed to include everything I needed to do this in a user friendly manner.

# Disclaimer
I am not a lisp/schemer wizard, I am going to get things wrong, they are going to be messy and I will still get the results. My background is Python and Pascal, so if you have any suggestions as to how to improve the code to solve the puzzles please let me know!